
const TS = Math.random() + 'ab' + Math.random();
const HASH = md5(TS+PRIVATE + API) // helped with md.min.js library
const KEY = `ts=${TS}&apikey=${API}&hash=${HASH}`
offset = 0;
lowCharacters = [];
let characterChoice;
let activeCharacter;
const comicDiv = document.getElementById('comics');


let characterBuild = setInterval(function () {
    const BASEURL = `http://gateway.marvel.com/v1/public/characters?offset=${offset}&limit=100&`
    
   fetch(BASEURL + KEY)
        .then((response) => {
        return response.json();
        })
        .then((data) => {
            const results = data.data.results;
           results.forEach( (character) => {
                if (character.comics.available < 5 && character.comics.available > 0 ) {
                    lowCharacters.push(character)
                }
            })
        })
    offset += 100;
    if (offset === 1500) { clearInterval(characterBuild);}
}, 500)

const chooseCharacter = () => {
    if (lowCharacters.length < 19) {
        setTimeout(switchCharacter , 1500); // Allows for array to populate if user wants to push button quickly
    }
    else {
        switchCharacter()
    }
    function switchCharacter () {
        const i = Math.floor(Math.random()*lowCharacters.length);
        const c = lowCharacters[i];
        characterChoice = JSON.stringify(c);
        localStorage.setItem('CHARACTER', characterChoice)
        characterDisplay(characterChoice)
    }
}

class Character {
    constructor(name, size,image,comics) {
        this.name= name;
        this.size= size;
        this.image= image;
        this.comics = comics;
    }
    showCollection() {
        comicDiv.innerHTML = '';
        const items = this.comics.items;
        items.forEach( function(comic) {
        const uri = comic.resourceURI + '?'
        fetch(uri + KEY)
            .then((response) => {
                return response.json();
            })
            .then((data) => {
                const d = data.data.results[0];
                const title = d.title;
                const div = document.createElement('div');
                div.setAttribute('class','cb')
                const comicImg = d.thumbnail.path + '.' + d.thumbnail.extension;
                let string = `<img src='${comicImg}' /><h3>${title}</h3>`;
                if (d.description !== null) {string += `<p>${d.description}<p>`}
                comicDiv.appendChild(div);              
                div.innerHTML = string;
            })
        })
    }
}



const characterDisplay = (d) => {

    const c = JSON.parse(d);
    const desc = c.description;
    const name = c.name;
    const comics = c.comics;
    const comicNum = comics.available;
    const image = c.thumbnail.path + '.' + c.thumbnail.extension;
    activeCharacter = new Character(name, comicNum,image,comics);
    comicDiv.innerHTML = '';
    document.getElementById('choose').innerText='Choose another obscure character';
    const heroDiv = document.getElementById('character');
    let divString = `<h2>${name}</h2>
    <img src='${image}' />
    <p>${desc}</p>`
    comicNum === 1 ? divString += `<p>${name} appears in 1 comic.</p>` :  divString += `<p>${name} appears in ${comicNum} comics.</p>`
    heroDiv.innerHTML = divString;
    document.getElementById('show').classList.remove('hidden');
    document.getElementById('holder').classList.remove('hidden');
}

characterChoice = localStorage.getItem('CHARACTER');

if (characterChoice !== null && characterChoice !== 'undefined') { 
    characterDisplay(characterChoice);
}

document.getElementById('choose').addEventListener('click',function() {
    chooseCharacter()
})

document.getElementById('show').addEventListener('click',function() {
    activeCharacter.showCollection();
    document.getElementById('show').classList.add('hidden');
})